<?php
require_once("../../../vendor/autoload.php");
use App\Hobbies\Hobbies;

$objHobbies = new Hobbies();


$objHobbies->setStudentName($_POST['student_name']);
$objHobbies->setStudentHobbies(implode(',', $_POST['hobby_list']));
$objHobbies->store();