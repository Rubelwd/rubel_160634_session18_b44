<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Insert Data Into Database</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <form action="store.php" method="post">
        <div class="form-group">
            <label for="applicant_name">Applicant's Name</label>
            <input type="text" class="form-control" name="student_name">
        </div>
        <div class="checkbox">
            Hobbies:
            <label>
                <input type="checkbox" name="hobby_list[]"> Playing Guiter
            </label>
            <label>
                <input type="checkbox" name="hobby_list[]"> Reading
            </label>
            <label>
                <input type="checkbox" name="hobby_list[]"> Painting
            </label>
            <label>
                <input type="checkbox" name="hobby_list[]"> Gardenning
            </label>
        </div>
        <input type="submit" value="Submit" name="submit_form" class="btn btn-default">
    </form>
</div>
</body>
</html>