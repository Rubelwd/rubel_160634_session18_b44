<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Insert Data Into Database</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <form action="store.php" method="post">
        <div class="form-group">
            <label for="city_name">Company Name</label>
            <input type="text" class="form-control" name="company_name">
        </div>
        <textarea class="form-control" rows="3" name="summery_of_org"></textarea>
        <input type="submit" value="Submit" name="submit_form" class="btn btn-default">
    </form>
</div>
</body>
</html>