<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Insert Data Into Database</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <style>

    </style>
</head>
<body>
<div class="container-fluid">
    <form action="store.php" method="post">
        <div class="form-group">
            <label for="book_name">Book Name</label>
            <input type="text" class="form-control" name="book_name">
        </div>
        <div class="form-group">
            <label for="author_name">Author Name</label>
            <input type="text" class="form-control" name="author_name">
        </div>
        <input type="submit" value="Submit" name="submit_form" class="btn btn-default">
    </form>
</div>

</body>
</html>