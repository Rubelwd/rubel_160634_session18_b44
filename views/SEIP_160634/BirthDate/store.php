<?php
require_once("../../../vendor/autoload.php");
use App\BirthDate\BirthDate;

$objBirthDate = new BirthDate();

$objBirthDate->setData($_POST);
$objBirthDate->store();