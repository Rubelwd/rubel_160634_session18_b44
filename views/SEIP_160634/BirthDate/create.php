<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Insert Data Into Database</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <form action="store.php" method="post">
        <div class="form-group">
            <label for="userName">User Name</label>
            <input type="text" class="form-control" name="user_name">
        </div>
        <div class="form-group">
            <label for="dateOfBirth">Date of Birth</label>
            <input type="date" class="form-control" name="birth_date">
        </div>
        <input type="submit" value="Submit" name="submit_form" class="btn btn-default">
    </form>
</div>

</body>
</html>