<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Insert Data Into Database</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<div class="container-fluid">
    <form action="store.php" method="post">
        <div class="form-group">
            <label for="applicant_name">Applicant's Name</label>
            <input type="text" class="form-control" name="applicant_name">
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="applicant_gender" value="Male" checked>Male
            </label>
            <label>
                <input type="radio" name="applicant_gender" value="Female" checked>Female
            </label>
        </div>
        <input type="submit" value="Submit" name="submit_form" class="btn btn-default">
    </form>
</div>
</body>
</html>