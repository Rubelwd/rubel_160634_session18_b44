<?php


namespace App\BookTitle;
use App\Model\Database as DB;


class BookTitle extends DB
{
    private $id;
    private $book_name;
    private $author_name;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
        $this->id = $postData['id'];
        }

        if(array_key_exists('book_name',$postData)) {
            $this->book_name = $postData['book_name'];
        }

        if(array_key_exists('author_name',$postData)) {
            $this->author_name = $postData['author_name'];
        }
    }

    public function store() {
        $arrData = array($this->book_name,$this->author_name);
        $sql = "INSERT INTO book_title (book_name,author_name) VALUES(?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
    }


}