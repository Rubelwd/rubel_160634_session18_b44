<?php


namespace App\Email;
use App\Model\Database as DB;


class Email extends DB
{
    private $id;
    private $email_address;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
        $this->id = $postData['id'];
        }

        if(array_key_exists('email_address',$postData)) {
            $this->email_address = $postData['email_address'];
        }
    }

    public function store() {
        $arrData = array($this->email_address);
        $sql = "INSERT INTO email (email_address) VALUES (?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
    }


}