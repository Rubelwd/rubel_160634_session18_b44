<?php


namespace App\Gender;
use App\Model\Database as DB;


class Gender extends DB
{
    private $id;
    private $applicant_name;
    private $applicant_gender;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
        $this->id = $postData['id'];
        }

        if(array_key_exists('applicant_name',$postData)) {
            $this->applicant_name = $postData['applicant_name'];
        }

        if(array_key_exists('applicant_gender',$postData)) {
            $this->applicant_gender = $postData['applicant_gender'];
        }
    }

    public function store() {
        $arrData = array($this->applicant_name,$this->applicant_gender);
        $sql = "INSERT INTO gender (applicant_name,applicant_gender) VALUES (?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
    }


}