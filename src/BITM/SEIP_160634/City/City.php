<?php


namespace App\City;
use App\Model\Database as DB;


class City extends DB
{
    private $id;
    private $city_name;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
        $this->id = $postData['id'];
        }

        if(array_key_exists('city_name',$postData)) {
            $this->city_name = $postData['city_name'];
        }
    }

    public function store() {
        $arrData = array($this->city_name);
        $sql = "INSERT INTO city (city_name) VALUES (?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
    }


}