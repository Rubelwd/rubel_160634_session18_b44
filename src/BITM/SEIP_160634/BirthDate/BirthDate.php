<?php

namespace App\BirthDate;
use App\Model\Database as DB;


class BirthDate extends DB
{
    private $id;
    private $user_name;
    private $birth_date;

    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
            $this->id = $postData['id'];
        }

        if(array_key_exists('user_name',$postData)) {
            $this->user_name = $postData['user_name'];
        }

        if(array_key_exists('birth_date',$postData)) {
            $this->birth_date = $postData['birth_date'];
        }
    }

    public function store() {
        $arrData = array($this->user_name,$this->birth_date);
        $sql = "INSERT INTO birth_date (user_name,birth_date) VALUES(?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
    }
}