<?php


namespace App\Hobbies;
use App\Model\Database as DB;


class Hobbies extends DB
{
    private $id;
    private $student_name;
    private $student_hobbies;

    public function setStudentName($student_name)
    {
        $this->student_name = $student_name;
    }

    public function setStudentHobbies($student_hobbies)
    {
        $this->student_hobbies = $student_hobbies;
    }



    public function store() {
        $arrData = array($this->student_name,$this->student_hobbies);
        $sql = "INSERT INTO gender (student_name,student_hobbies) VALUES (?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);
    }


}